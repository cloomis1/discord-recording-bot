# Discord Recording bot

This is a Discord screenshare recording bot for the IDF Class. It uses some discord api functionality as well as xdotool and obs to record videos. 

## Use instructions:

1. Make a new discord account. You will need it to be seperate from your normal account, as it can only watch one channel at a time right now. **Note:** if using gmail, you can use a [task-specific email address](https://support.google.com/a/users/answer/9308648?hl=en) and use the same mailbox.
2. Follow the instructions for [creating a bot account](https://discordpy.readthedocs.io/en/latest/discord.html)
3. **This should be run in a graphical Linux (Debian-based) VM.** This bot moves windows on your screen, which can be annoying if it is not running in a VM. 

Install the dependencies into your VM:
```bash
apt -y install obs-studio python3 python3-pip python3-opencv xdotool wmctrl

pip3 install pytesseract discord.py asyncio pyscreenshot datetime

snap install discord
```

4. Login to you Discord with your new account (not the bot).
5. Import the profile (Profile -> import) from the obs-settings/profile directory in this repo. 
6. Import the scene (Scene Collection -> import ) If you are not using Linux, you will need to set up the scene on your own.
7. Get your bot's access token from discord
8. Paste your access token into a file named secret.txt in the same directory as bot.py.
9. Invite your bot to the server where you want to run the commands. 
10. Have your regular account watch the voice chat you want to record. You should be able to see all users in the channel. See below image. (in the case of IDF, it is classroom-1)
[](screenshot.png)
11. Run `python3 bot.py` in the terminal for your VM.