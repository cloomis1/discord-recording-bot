import os, discord, pyscreenshot, asyncio
import cv2

try:
    from PIL import Image, ImageOps
except ImportError:
    import Image

import pytesseract
from pytesseract import Output
from discord.ext import commands, tasks
from datetime import datetime

bot = commands.Bot(command_prefix='~')
token = open("secret.txt", 'r').read() #Token was seved here

bot.currently_recording = False
bot.auto_detect_stream = False
bot.on_break = False
#Lacks output filename. Added in the record function.
recording_cmd = open("cmd.txt", 'r').read().strip()

bot.recording_process = None
bot.bot_channel = None

client = discord.Client()

#This runs one time when connected to discord
@bot.event
async def on_ready():
    print("Connected to discord")
    await bot.change_presence(activity=discord.Game(name="Waiting"))
    bot.loop.create_task(check_status_update())

#This is the guide that runs when ~guide is called
@bot.command(name="guide", help="Usage Guide")
async def guide(ctx):
    bot.bot_channel = ctx
    print("Guide called")
    help = """This is a discord bot that will record your voice and stream.

**Step 1.** Join the channel you want to record.
**Step 2.** Share your screen (if you want)
**Step 3.** Do ~voice or ~stream to record your channel or your streamed screenshare.
**Step 4.** Do ~stop to stop the recording.

Other Commands
**~autoOn** - Turns on automatic stream detection - Will record if a stream is noticed in classroom-1. Will stop recording after stream ends.
**~autoOff** - Turns off automatic stream detection
**~break** - toggles automatic stream recording for arg minutes. Example: ~break 10
**~endBreak** - Ends break early
**~status** - Replies with the current recording status
"""

    await ctx.channel.send(help)

# Records voice
@bot.command(name="voice", help="Records your current voice channel")
async def join(ctx):
    bot.bot_channel = ctx
    print ("Voice Called...")
    open_window(ctx)
    res = record(ctx)
    if res=="Recording Started.":
        await bot.change_presence(activity=discord.Game(name="Recording"))
        await ctx.channel.send(res)
        keystroke()
    else:
        await ctx.channel.send(res)


# Records screen (manual)
@bot.command(name="stream", help="Records your live screenshare")
async def stream(ctx):
    print ("Voice Called...")
    if (bot.auto_detect_stream):
        open_window(ctx)
        find_livestream()
        res = record(ctx)
        if res=="Recording Started.":
            await bot.change_presence(activity=discord.Game(name="Recording"))
            await ctx.channel.send(res)
            keystroke()
        else:
            await ctx.channel.send(res)
    else:
        await ctx.channel.send("~stream and ~stop are manual commands not compatable with automatic stream detection. To stop recording, stop screenshare instead.") 

#Stops Recording (Manual)
@bot.command(name="stop", help="Stops recording")
async def stop(ctx):
    print ("Stop Called")
    if (bot.auto_detect_stream):
        if bot.currently_recording:
            await ctx.channel.send("Recording Stopped.")
            keystroke()
            bot.currently_recording = False
            await bot.change_presence(activity=discord.Game(name="Waiting"))
        else:
            await ctx.channel.send("Recording not found. Are you sure that it's started?")
    else:
        await ctx.channel.send("~stream and ~stop are manual commands not compatable with automatic stream detection. To stop recording, stop screenshare instead.") 

#Toggles automatic recording on.
@bot.command(name="autoOn", help="Toggles automatic recording on")
async def join(ctx):
    bot.bot_channel = ctx
    bot.auto_detect_stream = True
    await bot.bot_channel.channel.send("Screenshare detection toggled on!")


#Toggles Automatic recording off
@bot.command(name="autoOff", help="Toggles automatic recording off")
async def join(ctx):
    bot.bot_channel = ctx
    bot.auto_detect_stream = False
    await bot.bot_channel.channel.send("Screenshare detection toggled off!")
    if bot.currently_recording:
        await bot.bot_channel.channel.send("Stopping current recording")
        keystroke()

#Responds to the command with the current recording status.
@bot.command(name="status", help="Replies to inform you if the bot is recording or not.")
async def status(ctx):
    if bot.currently_recording:
        await bot.bot_channel.channel.send("Currently recording")
    else:
        await bot.bot_channel.channel.send("Currently not recording")

#Responds to the command with the current recording status.
@bot.command(name="break", help="Toggles automatic stream recording for x minutes. Usage: ~break 10")
async def take_break(ctx, arg):
    time = None
    if bot.currently_recording:
        await ctx.channel.send("Currently recording. Toggling off")
        keystroke()
    
    try:
        time = int(arg) * 60
    except ValueError:
        await ctx.channel.send("Expected number only. Ex: ~break 10. Try again.")
    
    if not bot.on_break:
        if time:
            await ctx.channel.send("Taking a " + arg + " minute break.")
            await bot.change_presence(activity=discord.Game(name="Classroom Break"))
            bot.on_break = True
            await asyncio.sleep(time)
            if bot.on_break:
                bot.on_break = False
                await ctx.channel.send("Break ended.")
    else:
        await ctx.channel.send("Bot already on break. ~endBreak first")
    
#Responds to the command with the current recording status.
@bot.command(name="endBreak", help="Ends break early")
async def take_break(ctx):
    if bot.on_break:
        bot.on_break = False
        await ctx.channel.send("Break ended.")
    else:
        await ctx.channel.send("Bot not on break.")
    
# This function will take a screenshot every ten seconds. It then uses tesseract OCR to detect the words either close stream or
# watch stream, depending on if the stream is open. I understand that this is a strange implementation, but discord API documentation has 
# nothing on goLive.
async def check_status_update():
    while True:
        await asyncio.sleep(5)
        if (bot.auto_detect_stream and not bot.on_break):
            print ("Checking Status")
            precheck = bot.currently_recording
            find_livestream()
            if not (precheck == bot.currently_recording):
                if bot.currently_recording:
                    await bot.bot_channel.channel.send("Screenshare detected. Recording!")
                    keystroke()
                    print ("Screenshare detected. Recording!")
                    await bot.change_presence(activity=discord.Game(name="Recording"))
                else:
                    await bot.bot_channel.channel.send("Screenshare stopped. Stopping recording.")
                    keystroke()
                    print ("Screenshare stopped. Stopping recording.")
                    await bot.change_presence(activity=discord.Game(name="Waiting"))

# Checks Opens the discord window
def open_window(ctx):
    window = os.popen("wmctrl -l | grep Discord").read().strip()
    print (window)
    if window:
        #print ("... Connected")
        os.system("wmctrl -a Discord")
        id = os.popen("xdotool getactivewindow").read().strip()
        os.system("xdotool windowsize " + id + " 100% 100%")

    else:
        print ("... Failed")
        try:
            ctx.channel.send("Discord not opened on recording computer - Contact Caleb.")
        except:
            pass

def record(ctx):
    if not bot.currently_recording:
        print ("starting recording...")
        bot.currently_recording = True
        return ("Recording Started")
    else:
        return ("Already recording. Please stop recording first.")

def find_livestream():
    open_window(None)
    img = pyscreenshot.grab()
    img.save('screen.png')

    words = ocr_core("screen.png")
    coords = getStreamCoords(words)
    if coords:
        click(coords)
        bot.currently_recording = not bot.currently_recording

#Returns the pixel coords of the first location of the words Watch or Stream.
def getStreamCoords(words):
    wordlist=list()
    wordlist.append("Stream")
    if bot.currently_recording:
        wordlist.append("Watch")
    else:
        wordlist.append("Close")

    n_boxes = len(words['level'])
    try:
        for i in range (n_boxes):
            text = words['text'][i]
            if (text in wordlist):
                found = True
                return (str(words['left'][i]) + ", " + str(words['top'][i]))
    except:
        return None

def click(coords):
    os.system("xdotool mousemove " + coords + " click 1")

def ocr_core(filename):
    """
    This function will handle the core OCR processing of images.
    Turns file to black and white, turns the white >=230 into black and the rest white.
    Then returns all detected words in a dictiomnary
    """
    column = Image.open(filename)
    gray = column.convert('L')
    blackwhite = gray.point(lambda x: 255 if x < 230 else 0, '1')
    blackwhite.save("code_bw.jpg")
    data = pytesseract.image_to_data(blackwhite, output_type=Output.DICT, lang='eng')
    return data

def keystroke():
    os.system("xdotool keydown 0xffe1 keydown 0xffe3 keydown 0xffb9; sleep 0.1; xdotool keyup 0xffe3 keyup 0xffe1 keyup 0xffb9")

bot.run(token)
